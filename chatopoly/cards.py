# -*- coding: utf-8 -*-
# Built using https://answers.yahoo.com/question/index?qid=20110528154141AAFwRyu
from tile import *

class Cardlist(object):
    def __init__(self):
        self.cards = []

class CommunityChest(Cardlist):
    def __init__(self):
        super(CommunityChest, self).__init__()

        self.cards.append(CommunityChest(
            "Pay Hospital {}100"
        ))
        self.cards.append(CommunityChest(
            "Pay School Tax of {}150"
        ))
        self.cards.append(CommunityChest(
            "Receive For Services {}25"
        ))
        self.cards.append(CommunityChest(
            "You Inherit {}100"
        ))
        self.cards.append(CommunityChest(
            "From Sale of Stock You Get {}45"
        ))

        self.cards.append(CommunityChest(
            "Advance to Go",
            "Collect {}200"
        ))
        self.cards.append(CommunityChest(
            "Bank Error In Your Favor",
            "Collect {}200"
        ))
        self.cards.append(CommunityChest(
            "Doctor's Fee",
            "Pay {}50"
        ))
        self.cards.append(CommunityChest(
            "Get Out of Jail Free",
            "This card may be kept until needed, or sold"
        ))
        self.cards.append(CommunityChest(
            "Go to Jail",
            "Go Directly to Jail – Do Not Pass Go, Do Not Collect {}200"
        ))
        self.cards.append(CommunityChest(
            "Grand Opera Opening",
            "Collect {}50 From Every Player for Opening Night Seats"
        ))
        self.cards.append(CommunityChest(
            "Income Tax refund",
            "Collect {}20"
        ))
        self.cards.append(CommunityChest(
            "Life Insurance Matures",
            "Collect {}100"
        ))
        self.cards.append(CommunityChest(
            "You are Assessed for Street Repairs",
            "{}40 per House, {}115 per Hotel"
        ))
        self.cards.append(CommunityChest(
            "You Have Won Second Prize in a Beauty Contest",
            "Collect {}10"
        ))
        self.cards.append(CommunityChest(
            "Xmas Fund Matures",
            "Collect {}100"
        ))


class Chance(Cardlist):
    def __init__(self):
        super(Chance, self).__init__()

        self.cards.append(Chance(
            "Pay poor tax of {}15"
        ))
        self.cards.append(Chance(
            "Advance to Illinois Ave."
        ))
        self.cards.append(Chance(
            "Go back 3 spaces"
        ))
        self.cards.append(Chance(
            "Bank pays you dividend of {}50"
        ))

        self.cards.append(Chance(
            "Advance token to nearest Utility.",
            "If unowned, you may buy it from the Bank. If owned, throw dice and pay owner a total ten times the amount thrown."
        ))
        self.cards.append(Chance(
            "Advance token to the nearest Railroad",
            "Pay owner twice the rental to which he/she is otherwise entitled. If Railroad is unowned, you may buy it from the Bank."
        ))
        self.cards.append(Chance(
            "Advance token to the nearest Railroad",
            "Pay owner twice the rental to which he/she is otherwise entitled. If Railroad is unowned, you may buy it from the Bank."
        ))
        self.cards.append(Chance(
            "Advance to St. Charles Place",
            "If You Pass Go, Collect {}200"
        ))
        self.cards.append(Chance(
            "Get out of Jail free",
            "This card may be kept until needed, or traded/sold"
        ))
        self.cards.append(Chance(
            "Go directly to Jail",
            "do not pass Go, do not collect {}200"
        ))
        self.cards.append(Chance(
            "Make general repairs on all your property",
            "For each house pay {}25 – for each hotel {}100"
        ))
        self.cards.append(Chance(
            "Take a trip to Reading Railroad",
            "If you pass Go collect {}200"
        ))
        self.cards.append(Chance(
            "Take a Walk on the Board Walk",
            "Advance Token to Board Walk"
        ))
        self.cards.append(Chance(
            "You have been elected chairman of the board",
            "Pay each player {}50"
        ))
        self.cards.append(Chance(
            "Your Building And Loan Matures",
            "Collect {}150"
        ))
        self.cards.append(Chance(
            "Advance to Go",
            "Collect {}200"
        ))
